#### 实验五 逻辑回归算法实验

##### 知识点回归

   逻辑回归是一种广义的线性回归模型，属于机器学习中的有监督的学习方式。其推导过程与计算方式类似于回归的过程，但和线线性回归不同，逻辑回归主要是用来解决二分类问题（也可以解决多分类问题）。通过给定的n组数据（训练集）来训练模型，并在训练结束后对给定的一组或多组数据（测试集）进行分类。

##### （1）逻辑回归红酒分类问题

使用SK-Learn自带的逻辑回归算法函数进行多分类分析，使用SK-Learn自带的红酒数据集。


```
from sklearn.datasets import load_wine
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# 加载数据集
wind_data = load_wine()
x = wind_data.data
y = wind_data.target
# 拆分数据集
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=10)

# 创建模型
lr = LogisticRegression(
    solver='saga',            #快速梯度下降法
    multi_class='ovr',        #多分类问题
    penalty='l2',             #采用L2正则化避免过拟合  正则化 （目的：避免模型过拟合）
    max_iter=10000,           #梯度下降最大迭代次数
)
# 训练模型
lr.fit(x_train, y_train)

# 模型预测
lr_predict = lr.predict(x_test)
# 模型评价
a_score = accuracy_score(y_test, lr_predict)
print("红酒数据集分类准确性: ", a_score)
```
输入代码，观察输出结果。

##### （2）参数调优

    从输出结果来看，虽然模型的准确率已经很不错了，但是我们仍然可以对其进行优化。

正则化选择参数 penalty 我们考虑使用 L2 正则化，优化算法选择参数 solver 使用随机平均梯度下降(sag)，分类方式选择参数 multi_class 使用 ovr，类型权重参数class_weight 

使用 balanced，随机种子为200。

```
from sklearn.datasets import load_wine
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# 加载数据集
wind_data = load_wine()
x = wind_data.data
y = wind_data.target
# 拆分数据集
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=200)

# 创建模型
lr = LogisticRegression(
    solver='sag',          #采用随机梯度下降
    multi_class='ovr',     #多分类问题
    penalty='l2',          #采用L2正则化避免过拟合
    max_iter=10000,        #梯度下降最大迭代次数
    class_weight='balanced',  #为样本值进行加权操作，实现样本值均衡，避免离散值或者离群值影响分类结果
)
# 训练模型
lr.fit(x_train, y_train)

# 模型预测
lr_predict = lr.predict(x_test)
# 模型评价
a_score = accuracy_score(y_test, lr_predict)
print("红酒数据集分类准确性:: ", a_score)
```
输入代码，观察结果。

通过设置逻辑回归的参数调优，可以看到逻辑回归算法的分类准确性明显提高。
  优化算法选择参数：solver参数决定了我们对逻辑回归损失函数的优化方法，有4种算法可以选择，分别是：

　 a) liblinear：使用了开源的liblinear库实现，内部使用了坐标轴下降法来迭代优化损失函数。

 　b) lbfgs：拟牛顿法的一种，利用损失函数二阶导数矩阵即海森矩阵来迭代优化损失函数。

　 c) newton-cg：利用损失函数二阶导数矩阵即海森矩阵来迭代优化损失函数。

　 d) sag：即随机平均梯度下降，是梯度下降法的变种，和普通梯度下降法的区别是每次迭代仅仅用一部分的样本来计算梯度，适合于样本数据多的时候，速度远比SGD快。
  
     multi_class参数决定了我们分类方式的选择，有 ovr和multinomial两个值可以选择，默认是 ovr。
   
     LogisticRegression和LogisticRegressionCV默认就带了正则化项。penalty参数可选择的值为"l1"和"l2".分别对应L1的正则化和L2的正则化，默认是L2的正则化。
   
     class_weight参数用于标示分类模型中各种类型的权重，可以不输入，即不考虑权重，或者说所有类型的权重一样。一般我们可以选择balanced让类库自己计算类型权重。