#### 实验九 KNN（最近K近邻）算法

##### 在实验中我们使用鸢尾花数据集进行测试
##### 在进行KNN算法时，最重要的是决定K值大小。


```
from sklearn.datasets import load_iris
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier

#读取鸢尾花数据集
iris = load_iris()
x = iris.data
y = iris.target
```
我们设置一个循环，采用交叉验证的方法查看一下，KNN算法中不同K取值时的模型误差。


```
k_range = range(1, 31)
k_error = []
#循环，取k=1到k=31，查看误差效果
for k in k_range:
    knn = KNeighborsClassifier(n_neighbors=k)
    #cv参数决定数据集划分比例，这里是按照5:1划分训练集和测试集
    scores = cross_val_score(knn, x, y, cv=6, scoring='accuracy')
    k_error.append(1 - scores.mean())

```
画图，x轴为k值，y值为误差值


```
plt.plot(k_range, k_error)
plt.xlabel('Value of K for KNN')
plt.ylabel('Error')
plt.show()
```
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20220817161856.png)

我们能明显看出K值取多少的时候误差最小，这里明显是K=11最好。当然在实际问题中，如果数据集比较大，那为减少训练时间，K的取值范围可以缩小。


```
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets

# 设置K=11
n_neighbors = 5

# 导入数据
iris = datasets.load_iris()
x = iris.data[:, :2]  # 实验中只采用前两个特征,方便画图在二维平面显示
y = iris.target
h = .02  # 网格中的步长
# 创建彩色的图
cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

#在for循环中进行KNN分类，同时画出分类效果图
for weights in ['uniform', 'distance']:
    # 创建了一个knn分类器的实例，并拟合数据。
    knn = neighbors.KNeighborsClassifier(n_neighbors, weights=weights)
    knn.fit(x, y)

    # 绘制决策边界。为此，我们将为每个分配一个颜色
    # 来绘制网格中的点 [x_min, x_max]x[y_min, y_max].
    x_min, x_max = x[:, 0].min() - 1, x[:, 0].max() + 1
    y_min, y_max = x[:, 1].min() - 1, x[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    Z = knn.predict(np.c_[xx.ravel(), yy.ravel()])
    # 绘制训练点
    plt.scatter(x[:, 0], x[:, 1], c=y, cmap=cmap_bold)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.title("3-Class classification (k = %i, weights = '%s')"
              % (n_neighbors, weights))
plt.show()
```
![输入图片说明](../QQ%E6%88%AA%E5%9B%BE20220817162016.png)

##### 划分数据集进行预测  使用SK-learn自带的算法包去实现KNN算法

```
from sklearn.model_selection import train_test_split
iris = datasets.load_iris()    # 导入数据和标签
iris_x = iris.data
iris_y = iris.target
x_train, x_test, y_train, y_test = train_test_split(iris_x, iris_y, test_size=0.3)  # 划分为训练集和测试集数据
knn = KNeighborsClassifier(n_neighbors, weights=weights) # 设置knn分类器
knn.fit(x_train, y_train)  # 进行训练
y_pre = knn.predict(x_test)
print("KNN分类准确性为：{:.3f}".format(knn.score(x_test, y_test)))
```
##### 思考：输入上述代码，观察KNN算法对于鸢尾花数据集的分类准确率。