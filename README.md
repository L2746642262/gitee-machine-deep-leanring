# 机器学习实践 

#### 实验列表
##### 机器学习部分
1.  [机器学习环境搭建](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/1.%E6%9C%BA%E5%99%A8%E5%AD%A6%E4%B9%A0%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA.md)
2.  [数据标准化](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/2.%E6%95%B0%E6%8D%AE%E6%A0%87%E5%87%86%E5%8C%96.md)
3.  [交叉验证与混淆矩阵](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/3.%20%E6%A8%A1%E5%9E%8B%E8%AF%84%E4%BC%B0.md)
4.  [线性回归](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/4.%E7%BA%BF%E6%80%A7%E5%9B%9E%E5%BD%92.md)
5.  [逻辑回归](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/6.%20%E9%80%BB%E8%BE%91%E5%9B%9E%E5%BD%92.md)
6.  [梯度下降](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/5.%E6%A2%AF%E5%BA%A6%E4%B8%8B%E9%99%8D.md)
7.  [决策树](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/7.%20%E5%86%B3%E7%AD%96%E6%A0%91%E5%AE%9E%E9%AA%8C.md)
8.  [随机森林](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/8.%20%E9%9A%8F%E6%9C%BA%E6%A3%AE%E6%9E%97.md)
9.  [KNN（最近K邻居）](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/9.%20KNN%E5%AE%9E%E9%AA%8C.md)
10.  [贝叶斯](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/10.%E8%B4%9D%E5%8F%B6%E6%96%AF%E7%AE%97%E6%B3%95.md)
11. [支持向量机](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/11.%20%E6%94%AF%E6%8C%81%E5%90%91%E9%87%8F%E6%9C%BA%E7%AE%97%E6%B3%95.md)
12. [神经网络](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/12.%20%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C.md)
13. [层次聚类](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/13%20%E5%B1%82%E6%AC%A1%E8%81%9A%E7%B1%BB.md)
14. [K-Means聚类](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/14.K-Means%E8%81%9A%E7%B1%BB.md)
15. [DBSCAN聚类](https://gitee.com/niit_edu_cn_0/gitee-machine-deep-leanring/blob/master/experimental%20%20documents/15.DBSCAN%E8%81%9A%E7%B1%BB.md)

#### 课程说明

1.  实验课时：20
2.  总学时：40
3.  任课教师: 夏吉安（计算机与软件学院）
4.  E-mail：xiagyan@niit.edu.cn
5.  手机：13851814532
5.  课程周数：1-7周